package ar.fiuba.tdd.tp1.api;

import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.actions.factory.ActionFactory;
import ar.fiuba.tdd.tp1.actions.sheet.CreateSheetAction;
import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;
import ar.fiuba.tdd.tp1.queries.Query;
import ar.fiuba.tdd.tp1.queries.cell.CellFormulaQuery;
import ar.fiuba.tdd.tp1.queries.cell.CellValueQuery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SpreadSheetTest {


    private static String DEFAULT_BOOKNAME = "default";
    private SpreadSheet spreadSheet;
    private TestAction action;

    @Before
    public void before() {
        spreadSheet = new SpreadSheet();
    }

    @Test
    public void createBook() {
        spreadSheet.createBook(DEFAULT_BOOKNAME);
        TestAction testAction = new TestAction(null, null, null);
        spreadSheet.execute(DEFAULT_BOOKNAME, testAction);
        Assert.assertNotNull(testAction.getBook());
    }

    @Test
    public void createBookWithName() {
        String name = "Test name";
        spreadSheet.createBook(name);
        TestAction testAction = new TestAction(null, null, null);
        spreadSheet.execute("Test name", testAction);
        Assert.assertNotNull(testAction.getBook());
        Assert.assertEquals(name, testAction.getBook().getName());
    }

    @Test
    public void doAndUndoAction() {
        spreadSheet.createBook(DEFAULT_BOOKNAME);
        TestAction testAction = new TestAction(null, null, null);
        spreadSheet.execute(DEFAULT_BOOKNAME, testAction);
        Assert.assertTrue(testAction.getDone());
        Assert.assertFalse(testAction.getUndone());
        spreadSheet.undo();
        Assert.assertTrue(testAction.getUndone());
        Assert.assertFalse(testAction.getDone());
    }

    @Test
    public void undoAndRedoAction() {
        spreadSheet.createBook(DEFAULT_BOOKNAME);
        TestAction testAction = new TestAction(null, null, null);
        spreadSheet.execute(DEFAULT_BOOKNAME, testAction);
        Assert.assertTrue(testAction.getDone());
        Assert.assertFalse(testAction.getUndone());
        spreadSheet.undo();
        Assert.assertTrue(testAction.getUndone());
        Assert.assertFalse(testAction.getDone());
        spreadSheet.redo();
        Assert.assertTrue(testAction.getDone());
        Assert.assertFalse(testAction.getUndone());

    }

    @Test
    public void undoAndRedoCleanRedoStack() {
        spreadSheet.createBook(DEFAULT_BOOKNAME);
        TestAction testAction = new TestAction(null, null, null);
        spreadSheet.execute(DEFAULT_BOOKNAME, testAction);
        Assert.assertTrue(testAction.getDone());
        Assert.assertFalse(testAction.getUndone());
        spreadSheet.undo();

        TestAction testAction2 = new TestAction(null, null, null);
        spreadSheet.execute(DEFAULT_BOOKNAME, testAction2);
        spreadSheet.redo();
        Assert.assertFalse(testAction.getDone());
        Assert.assertTrue(testAction.getUndone());
        Assert.assertTrue(testAction2.getDone());
        Assert.assertFalse(testAction2.getUndone());

    }

    @Test(expected = InexistentBookException.class)
    public void invalidBookTest() {
        TestAction testAction = new TestAction(null, null, null);
        spreadSheet.execute(DEFAULT_BOOKNAME, testAction);
    }

    @Test
    public void innerClassTest() {
        TestAction testAction = new TestAction(null, null, null);
        Assert.assertNull(testAction.getCell());
        Assert.assertNull(testAction.getSheet());
    }

    @Test
    public void addition() {
        SpreadSheet spreadSheet = new SpreadSheet();
        spreadSheet.createBook("book1");

        Action createSheet = new CreateSheetAction("SHEET1");
        spreadSheet.execute("book1", createSheet);
        Action cellAction = ActionFactory.cellAction("A", 1, "SHEET1").changeFormula("1");
        spreadSheet.execute("book1", cellAction);

        Action cellAction2 = ActionFactory.cellAction("A", 2, "SHEET1").changeFormula("3");
        spreadSheet.execute("book1", cellAction2);

        Action cellAction3 = ActionFactory.cellAction("A", 3, "SHEET1").changeFormula("= SHEET1!A1 + SHEET1!A2");
        spreadSheet.execute("book1", cellAction3);

        Query<String> query = new CellValueQuery("SHEET1", "A", 3);

        Assert.assertEquals("4.0", spreadSheet.execute("book1", query));

    }

    @Test(expected = InexistentBookException.class)
    public void getSheetsWithoutBook() {
        spreadSheet.getSheetsInBook(DEFAULT_BOOKNAME);
    }

    @Test
    public void getSheetsFromBook() {
        spreadSheet.createBook(DEFAULT_BOOKNAME);
        String name = "Sheet 1";
        Action addSheet = ActionFactory.bookAction().createSheet(name);
        spreadSheet.execute(DEFAULT_BOOKNAME, addSheet);
        Assert.assertTrue(spreadSheet.getSheetsInBook(DEFAULT_BOOKNAME).contains(name));
    }

    @Test
    public void functionsTest() {
        spreadSheet.createBookWithDefaultSheet(DEFAULT_BOOKNAME);
        Action firstAction = ActionFactory.cellAction("A", 1, "default").changeFormula("2");
        Action secondAction = ActionFactory.cellAction("A", 2, "default").changeFormula("10");
        Action thirdAction = ActionFactory.cellAction("A", 3, "default").changeFormula("= MAX(default!A1:A2)");
        Action fourthAction = ActionFactory.cellAction("A", 4, "default").changeFormula("= MIN(default!A1:A2)");
        Action fifthAction = ActionFactory.cellAction("A", 5, "default").changeFormula("= AVERAGE(default!A1:A2)");
        spreadSheet.execute(DEFAULT_BOOKNAME, firstAction);
        spreadSheet.execute(DEFAULT_BOOKNAME, secondAction);
        spreadSheet.execute(DEFAULT_BOOKNAME, thirdAction);
        spreadSheet.execute(DEFAULT_BOOKNAME, fourthAction);
        spreadSheet.execute(DEFAULT_BOOKNAME, fifthAction);
        Query<String> firstQuery = new CellValueQuery("default", "A", 3);
        Query<String> secondQuery = new CellValueQuery("default", "A", 4);
        Query<String> thirdQuery = new CellValueQuery("default", "A", 5);

        Assert.assertEquals("10.0", spreadSheet.execute(DEFAULT_BOOKNAME, firstQuery));
        Assert.assertEquals("2.0", spreadSheet.execute(DEFAULT_BOOKNAME, secondQuery));
        Assert.assertEquals("6.0", spreadSheet.execute(DEFAULT_BOOKNAME, thirdQuery));
    }

    @Test
    public void integrationFunctionTest() {
        spreadSheet.createBookWithDefaultSheet(DEFAULT_BOOKNAME);
        Action firstAction = ActionFactory.cellAction("A", 1, "default").changeFormula("2");
        Action secondAction = ActionFactory.cellAction("A", 2, "default").changeFormula("10");
        String complexFormula = "= MAX(default!A1:A2) + AVERAGE(default!A1:A2) - 2";
        Action thirdAction = ActionFactory.cellAction("A", 3, "default").changeFormula(complexFormula);
        spreadSheet.execute(DEFAULT_BOOKNAME, firstAction);
        spreadSheet.execute(DEFAULT_BOOKNAME, secondAction);
        spreadSheet.execute(DEFAULT_BOOKNAME, thirdAction);
        Query<String> firstQuery = new CellValueQuery("default", "A", 3);
        Query<String> secondQuery = new CellFormulaQuery("default", "A", 3);
        Assert.assertEquals("14.0", spreadSheet.execute(DEFAULT_BOOKNAME, firstQuery));
        Assert.assertEquals("= MAX(default!A1:A2) + AVERAGE(default!A1:A2) - 2.0", spreadSheet.execute(DEFAULT_BOOKNAME, secondQuery));
    }

    @Test
    public void simpleConcatCreation() {
        spreadSheet.createBookWithDefaultSheet(DEFAULT_BOOKNAME);
        Action firstAction = ActionFactory.cellAction("A", 1, "default").changeFormula("= CONCAT(\"hola\",\"chau\")");
        spreadSheet.execute(DEFAULT_BOOKNAME, firstAction);
        Query<String> query = new CellValueQuery("default", "A", 1);
        Assert.assertEquals("holachau", spreadSheet.execute(DEFAULT_BOOKNAME, query));
        Query<String> query2 = new CellFormulaQuery("default", "A", 1);
        Assert.assertEquals("= CONCAT(\"hola\",\"chau\")", spreadSheet.execute(DEFAULT_BOOKNAME, query2));
    }

    @Test
    public void concatReferenceTest() {
        spreadSheet.createBookWithDefaultSheet(DEFAULT_BOOKNAME);
        Action firstAction = ActionFactory.cellAction("A", 1, "default").changeFormula("= CONCAT(default!A2,\"chau\")");
        Action secondAction = ActionFactory.cellAction("A", 2, "default").changeFormula("caca");
        spreadSheet.execute(DEFAULT_BOOKNAME, secondAction);
        spreadSheet.execute(DEFAULT_BOOKNAME, firstAction);
        Query<String> query = new CellValueQuery("default", "A", 1);
        Assert.assertEquals("cacachau", spreadSheet.execute(DEFAULT_BOOKNAME, query));
        query = new CellFormulaQuery("default", "A", 1);
        Assert.assertEquals("= CONCAT(default!A2,\"chau\")", spreadSheet.execute(DEFAULT_BOOKNAME, query));
    }

    @Test
    public void concatRangeTest() {
        spreadSheet.createBookWithDefaultSheet(DEFAULT_BOOKNAME);
        Action action = ActionFactory.cellAction("A", 3, "default").changeFormula("= CONCAT(default!A1:B2,default!B3)");
        spreadSheet.execute(DEFAULT_BOOKNAME, action);
        action = ActionFactory.cellAction("A", 1, "default").changeFormula("caca");
        spreadSheet.execute(DEFAULT_BOOKNAME, action);
        action = ActionFactory.cellAction("A", 2, "default").changeFormula("1");
        spreadSheet.execute(DEFAULT_BOOKNAME, action);
        action = ActionFactory.cellAction("B", 1, "default").changeFormula("cacota");
        spreadSheet.execute(DEFAULT_BOOKNAME, action);
        action = ActionFactory.cellAction("B", 2, "default").changeFormula("2");
        spreadSheet.execute(DEFAULT_BOOKNAME, action);
        action = ActionFactory.cellAction("B", 3, "default").changeFormula("3");
        spreadSheet.execute(DEFAULT_BOOKNAME, action);
        Query<String> query = new CellValueQuery("default", "A", 3);
        Assert.assertEquals("cacacacota1.02.03.0", spreadSheet.execute(DEFAULT_BOOKNAME, query));
        query = new CellFormulaQuery("default", "A", 3);
        Assert.assertEquals("= CONCAT(default!A1:B2,default!B3)", spreadSheet.execute(DEFAULT_BOOKNAME, query));
    }

    /**
     * Action para hacer pruebas.
     * Created by jonathan on 23/09/15.
     */
    private class TestAction extends Action {

        private Boolean done = false;
        private Boolean undone = false;
        private String sheetId;
        private Integer row;
        private Integer column;
        private Cell cell;
        private Sheet sheet;

        public TestAction(String sheetId, Integer row, Integer column) {
            this.row = row;
            this.sheetId = sheetId;
            this.column = column;

        }


        @Override
        public void execute() {
            done = true;
            undone = false;
        }

        @Override
        public void undo() {
            undone = true;
            done = false;
        }


        public Book getBook() {
            return this.getContext();
        }

        public Boolean getDone() {
            return done;
        }

        public Boolean getUndone() {
            return undone;
        }

        public Cell getCell() {
            return cell;
        }

        public Sheet getSheet() {
            return sheet;
        }
    }

}
