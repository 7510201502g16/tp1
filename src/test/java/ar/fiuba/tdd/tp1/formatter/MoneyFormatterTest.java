package ar.fiuba.tdd.tp1.formatter;

import org.junit.Test;

import java.text.DecimalFormat;

import static org.junit.Assert.assertEquals;

/**
 * Created by jonathan on 14/10/15.
 */
public class MoneyFormatterTest {

    private static final char SEPARATOR = '.';

    @Test
    public void noDecimalsFormat() throws Exception {
        MoneyFormatter fm = MoneyFormatter.noDecimalsFormatter(CurrencySymbol.EURO);
        String result = fm.format("200.899");
        assertEquals(CurrencySymbol.EURO.getSymbol() + " 201", result);
    }

    @Test
    public void twoDecimalsFormat() {
        MoneyFormatter fm = MoneyFormatter.twoDecimalsFormatter(CurrencySymbol.AR);
        String result = fm.format("200.33333");
        assertEquals(CurrencySymbol.AR.getSymbol() + " 200" + SEPARATOR + "33", result);
    }

    @Test
    public void symbosTests() {
        MoneyFormatter fm = MoneyFormatter.twoDecimalsFormatter(CurrencySymbol.REAL);
        String result = fm.format("2");
        assertEquals(CurrencySymbol.REAL.getSymbol() + " 2" + SEPARATOR + "00", result);
        fm = MoneyFormatter.twoDecimalsFormatter(CurrencySymbol.US);
        result = fm.format("2");
        assertEquals(CurrencySymbol.US.getSymbol() + " 2" + SEPARATOR + "00", result);

        fm = MoneyFormatter.twoDecimalsFormatter(CurrencySymbol.YEN);
        result = fm.format("2");
        assertEquals(CurrencySymbol.YEN.getSymbol() + " 2" + SEPARATOR + "00", result);

    }

}
