package ar.fiuba.tdd.tp1.acceptance.driver;

import ar.fiuba.tdd.tp1.elements.formatter.Formatter;

/**
 * Created by jonathan on 28/10/15.
 */
public interface DefaultFormatterCreator {

    Formatter createDefault();
}
