package ar.fiuba.tdd.tp1.actions.factory;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test class for ValueContainer
 * Created by matias on 10/4/15.
 */
public class ValueContainerTest {

    @Test
    public void createValueContainer() {
        ValueContainer container = new ValueContainer();
        Assert.assertNotNull(container);
        Assert.assertNull(container.getValue());
    }

    @Test
    public void setValueToContainer() {
        ValueContainer container = new ValueContainer();
        Assert.assertNull(container.getValue());
        String value = "Prueba de value";
        container.setValue(value);
        Assert.assertEquals(value, container.getValue());
    }
}
