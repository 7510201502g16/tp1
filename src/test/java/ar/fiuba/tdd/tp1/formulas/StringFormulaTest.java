package ar.fiuba.tdd.tp1.formulas;

import ar.fiuba.tdd.tp1.elements.cell.Formula;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by matias on 9/25/15.
 */
public class StringFormulaTest {

    @Test
    public void getStringEvaluated() {
        String valor = "prueba";
        Formula stringFormula = new StringFormula(valor);
        Assert.assertEquals(valor, stringFormula.evaluate());
    }

    @Test
    public void getToStringFromString() {
        String valor = "prueba";
        Formula stringFormula = new StringFormula(valor);
        Assert.assertEquals(valor, stringFormula.toString());
    }

    @Test
    public void concatSimpleTest() {
        String valor1 = "valor";
        String valor2 = "concatenado";
        String valor3 = "exitoso";
        List<String> lista = new ArrayList<>();
        lista.add(valor1);
        lista.add(valor2);
        lista.add(valor3);
        Assert.assertEquals(valor1.concat(valor2).concat(valor3), RangeFunction.CONCAT.apply(lista));
    }
}
