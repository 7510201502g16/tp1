package ar.fiuba.tdd.tp1.formulas;

import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.actions.sheet.CreateSheetAction;
import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.cell.Formula;
import org.junit.Assert;
import org.junit.Test;

/**
 * BinaryFormula class tests
 * Created by matias on 9/25/15.
 */
public class BinaryOperationTest {

    @Test
    public void performAddition() {
        Formula firstFormula = new NumberFormula(2.0f);
        Formula secondFormula = new NumberFormula(3.0f);
        Formula additionFormula = new BinaryFormula(firstFormula, secondFormula, BinaryOperation.ADDITION);
        Assert.assertEquals(Float.parseFloat(additionFormula.evaluate()), 5.0f, 0.0001f);
    }

    @Test
    public void additionFormulaToString() {
        Formula firstFormula = new NumberFormula(2.0f);
        Formula secondFormula = new NumberFormula(3.0f);
        Formula additionFormula = new BinaryFormula(firstFormula, secondFormula, BinaryOperation.ADDITION);
        Assert.assertEquals(additionFormula.toString(), "2.0 + 3.0");
    }

    @Test
    public void performSubstraction() {
        Formula firstFormula = new NumberFormula(5.0f);
        Formula secondFormula = new NumberFormula(3.0f);
        Formula additionFormula = new BinaryFormula(firstFormula, secondFormula, BinaryOperation.SUBTRACTION);
        Assert.assertEquals(Float.parseFloat(additionFormula.evaluate()), 2.0f, 0.0001f);
    }

    @Test
    public void substractionFormulaToString() {
        Formula firstFormula = new NumberFormula(5.0f);
        Formula secondFormula = new NumberFormula(3.0f);
        Formula additionFormula = new BinaryFormula(firstFormula, secondFormula, BinaryOperation.SUBTRACTION);
        Assert.assertEquals(additionFormula.toString(), "5.0 - 3.0");
    }

    @Test
    public void performComplexFormula() {
        Formula firstFormula = new NumberFormula(5.0f);
        Formula secondFormula = new NumberFormula(3.0f);
        Formula thirdFormula = new NumberFormula(4.0f);
        Formula additionFormula = new BinaryFormula(firstFormula, secondFormula, BinaryOperation.ADDITION);
        Formula substractionForula = new BinaryFormula(additionFormula, thirdFormula, BinaryOperation.SUBTRACTION);
        Assert.assertEquals(Float.parseFloat(substractionForula.evaluate()), 4.0f, 0.0001f);
    }

    @Test
    public void complexFormulaToString() {
        Formula firstFormula = new NumberFormula(5.0f);
        Formula secondFormula = new NumberFormula(3.0f);
        Formula thirdFormula = new NumberFormula(4.0f);
        Formula additionFormula = new BinaryFormula(firstFormula, secondFormula, BinaryOperation.ADDITION);
        Formula substractionForula = new BinaryFormula(additionFormula, thirdFormula, BinaryOperation.SUBTRACTION);
        Assert.assertEquals(substractionForula.toString(), "5.0 + 3.0 - 4.0");
    }

    @Test
    public void valueOf() {
        BinaryOperation binaryOperation = BinaryOperation.valueOf("ADDITION");
        Assert.assertEquals(BinaryOperation.ADDITION, binaryOperation);
    }


    @Test(expected = IllegalArgumentException.class)
    public void valueOfInvalid() {
        BinaryOperation binaryOperation = BinaryOperation.valueOf("7");
        Assert.assertEquals(BinaryOperation.ADDITION, binaryOperation);
    }

    @Test
    public void complexFormulaWithReference() {
        Book book = new Book();
        String sheetName = "sheet1";
        Action createSheet = new CreateSheetAction(sheetName);
        createSheet.setContext(book);
        createSheet.execute();
        String referenceString = sheetName + "!A12";
        final ReferenceFormula referenceFormula = new ReferenceFormula(referenceString, book);
        Formula firstFormula = new NumberFormula(5.0f);
        Formula additionFormula = new BinaryFormula(firstFormula, referenceFormula, BinaryOperation.ADDITION);
        Assert.assertEquals("5.0", additionFormula.evaluate());
        Assert.assertEquals("5.0 + " + referenceString, additionFormula.toString());
    }
}
