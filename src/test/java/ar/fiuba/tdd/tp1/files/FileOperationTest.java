package ar.fiuba.tdd.tp1.files;

import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.queries.factory.QueryFactory;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by jonathan on 18/10/15.
 */
public class FileOperationTest {

    public static final String BOOK_1 = "book1";
    private SpreadSheet api;

    @Before
    public void before() {
        api = new SpreadSheet();
        api.createBook(BOOK_1);
    }

    @Test
    public void importSheet() {
        FileOperations.importSheet("files/csv/validsheet.csv", BOOK_1, "valid", api);
        assertTrue(api.getSheetsInBook(BOOK_1).contains("valid"));
        assertEquals("unvalor", api.execute(BOOK_1, QueryFactory.cellQuery("valid", "A", 1).getValue()));
        assertEquals("dosvalores", api.execute(BOOK_1, QueryFactory.cellQuery("valid", "B", 1).getValue()));
        assertEquals("otro valor", api.execute(BOOK_1, QueryFactory.cellQuery("valid", "C", 1).getValue()));
        assertEquals("1.0", api.execute(BOOK_1, QueryFactory.cellQuery("valid", "D", 1).getValue()));
        assertEquals("5.0", api.execute(BOOK_1, QueryFactory.cellQuery("valid", "D", 2).getValue()));
        assertEquals("0.0", api.execute(BOOK_1, QueryFactory.cellQuery("valid", "A", 3).getValue()));
        assertEquals("0.0", api.execute(BOOK_1, QueryFactory.cellQuery("valid", "B", 3).getValue()));
        assertEquals("0.0", api.execute(BOOK_1, QueryFactory.cellQuery("valid", "c", 3).getValue()));
    }

}
