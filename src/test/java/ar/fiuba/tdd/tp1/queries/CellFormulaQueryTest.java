package ar.fiuba.tdd.tp1.queries;

import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.actions.cell.ChangeCellFormulaAction;
import ar.fiuba.tdd.tp1.actions.sheet.CreateSheetAction;
import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.queries.factory.CellQueryFactory;
import ar.fiuba.tdd.tp1.queries.factory.QueryFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * CellFormulaQuery test cases.
 * Created by matias on 10/4/15.
 */
public class CellFormulaQueryTest {

    private static String SHEET_ID = "SHEET1";
    private Book book;

    @Before
    public void before() {
        book = new Book();
        Action sheetAction = new CreateSheetAction(SHEET_ID);
        sheetAction.setContext(book);
        sheetAction.execute();
        Action cellAction = new ChangeCellFormulaAction(SHEET_ID, "A", 10, "=2 + 5");
        cellAction.setContext(book);
        cellAction.execute();
    }

    @Test
    public void getCellValue() {
        CellQueryFactory cellQueryFactory = QueryFactory.cellQuery(SHEET_ID, "A", 10);
        Query<String> query = cellQueryFactory.getFormula();
        query.setContext(book);
        String formula = query.execute();
        Assert.assertEquals("= 2.0 + 5.0", formula);
    }

}
