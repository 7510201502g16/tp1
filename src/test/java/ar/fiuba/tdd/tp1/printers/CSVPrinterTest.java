package ar.fiuba.tdd.tp1.printers;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;
import ar.fiuba.tdd.tp1.formulas.formulacreator.FormulaCreator;
import ar.fiuba.tdd.tp1.printers.csv.CSVBookPrinter;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by maxi on 11/10/15.
 */
public class CSVPrinterTest {


    public static final String NEW_SHEET = "New sheet";
    private static final String FILE_NAME = "aCsv";
    private static final String FILE_EXTENSION = ".csv";
    private static final String FILE_PATH = File.separator + "tmp" + File.separator + FILE_NAME + FILE_EXTENSION;
    private static final String PRINT_NAME = File.separator + "tmp" + File.separator + FILE_NAME;

    @Test
    public void createCSVWithOneCell() throws FileNotFoundException {
        Book book = new Book();
        book.createSheet(NEW_SHEET);
        Sheet sheet = book.getSheet(NEW_SHEET);
        sheet.getCell("A", 1).setFormula(FormulaCreator.createFormulaFromInfix("1", book));
        CSVBookPrinter csvPrinter = new CSVBookPrinter(book, PRINT_NAME);
        csvPrinter.print();
        File file = new File(FILE_PATH);
        Assert.assertEquals(file.canRead(), true);
        String expectedResult = sheet.getCell("A", 1).getValue();
        Assert.assertEquals(new Scanner(file).useDelimiter("\\A").next(), expectedResult);
    }

    @Test
    public void createCSVWithMultiplesCells() throws FileNotFoundException {
        Book book = new Book();
        book.createSheet(NEW_SHEET);
        Sheet sheet = book.getSheet(NEW_SHEET);
        sheet.getCell("A", 1).setFormula(FormulaCreator.createFormulaFromInfix("1", book));
        sheet.getCell("B", 2).setFormula(FormulaCreator.createFormulaFromInfix("9", book));
        CSVBookPrinter csvPrinter = new CSVBookPrinter(book, PRINT_NAME);
        csvPrinter.print();
        File file = new File(FILE_PATH);
        Assert.assertEquals(file.canRead(), true);

        String expectedResult = "1.0,0.0" + "\n" + "0.0,9.0";

        Assert.assertEquals(new Scanner(file).useDelimiter("\\A").next(), expectedResult);
    }

    @Test
    public void createCSVWithStringCell() throws FileNotFoundException {
        Book book = new Book();
        book.createSheet(NEW_SHEET);
        Sheet sheet = book.getSheet(NEW_SHEET);
        sheet.getCell("A", 1).setFormula(FormulaCreator.createFormulaFromInfix("1", book));
        sheet.getCell("B", 2).setFormula(FormulaCreator.createFormulaFromInfix("Hello", book));
        CSVBookPrinter csvPrinter = new CSVBookPrinter(book, PRINT_NAME);
        csvPrinter.print();
        File file = new File(FILE_PATH);
        Assert.assertEquals(file.canRead(), true);

        String expectedResult = "1.0,0.0" + "\n" + "0.0,Hello";

        Assert.assertEquals(new Scanner(file).useDelimiter("\\A").next(), expectedResult);
    }

    @Test
    public void createCSVWithMultiplesSheets() {
        Book book = new Book();
        book.createSheet(NEW_SHEET);
        Sheet sheet = book.getSheet(NEW_SHEET);
        sheet.getCell("A", 1).setFormula(FormulaCreator.createFormulaFromInfix("1", book));
        sheet.getCell("B", 2).setFormula(FormulaCreator.createFormulaFromInfix("Hello", book));
        book.createSheet("Second sheet");
        Sheet otherSheet = book.getSheet("Second sheet");
        otherSheet.getCell("A", 1).setFormula(FormulaCreator.createFormulaFromInfix("1", book));
        book.setActiveSheet(NEW_SHEET);
        CSVBookPrinter csvPrinter = new CSVBookPrinter(book, PRINT_NAME);
        csvPrinter.print();
        File file = new File(FILE_PATH);
        Assert.assertEquals(file.canRead(), true);

        String expectedResult = "1.0,0.0" + "\n" + "0.0,Hello";


        try {
            Assert.assertEquals(new Scanner(file).useDelimiter("\\A").next(), expectedResult);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
