package ar.fiuba.tdd.tp1.elements;

import ar.fiuba.tdd.tp1.elements.cell.Formula;
import ar.fiuba.tdd.tp1.formulas.InvalidFormulaException;
import ar.fiuba.tdd.tp1.formulas.formulacreator.FormulaCreator;
import org.junit.Assert;
import org.junit.Test;

/**
 * Test del formula creator.
 * Created by matias on 9/25/15.
 */
public class FormulaCreatorTest {

    @Test
    public void createNumericFormula() {
        Formula formulaNumerica = FormulaCreator.createFormulaFromInfix("2", null);
        Assert.assertEquals(Float.parseFloat(formulaNumerica.evaluate()), 2.0f, 0.0001);
    }

    @Test
    public void createStringFormula() {
        String value = "Prueba";
        Formula formulaString = FormulaCreator.createFormulaFromInfix(value, null);
        Assert.assertEquals(formulaString.evaluate(), value);
    }

    @Test
    public void createComplexFormula() {
        Formula complexFormula = FormulaCreator.createFormulaFromInfix("=5 + 3 - 2", null);
        Assert.assertEquals(Float.parseFloat(complexFormula.evaluate()), 6.0f, 0.0001f);
    }

    @Test
    public void createComplexWithLeadingSpacesFormula() {
        Formula complexFormula = FormulaCreator.createFormulaFromInfix("= 5 + 3 - 2", null);
        Assert.assertEquals(Float.parseFloat(complexFormula.evaluate()), 6.0f, 0.0001f);
    }

    @Test
    public void createComplexWithTrailingSpacesFormula() {
        Formula complexFormula = FormulaCreator.createFormulaFromInfix("= 5 + 3 - 2  ", null);
        Assert.assertEquals(Float.parseFloat(complexFormula.evaluate()), 6.0f, 0.0001f);
    }

    @Test(expected = InvalidFormulaException.class)
    public void missingOperationsFormula() {
        Formula complexFormula = FormulaCreator.createFormulaFromInfix("= 5 + 3 -", null);
        complexFormula.evaluate();
    }

    @Test(expected = InvalidFormulaException.class)
    public void extraNumbersFormula() {
        Formula complexFormula = FormulaCreator.createFormulaFromInfix("= 2 2", null);
        complexFormula.evaluate();
    }

    @Test(expected = InvalidFormulaException.class)
    public void extraOperationsFormula() {
        Formula complexFormula = FormulaCreator.createFormulaFromInfix("= 2 +  + 2 ", null);
        complexFormula.evaluate();
    }

    @Test
    public void maxFormulaSimpleRangeCreation() {
        Formula formula = FormulaCreator.createFormulaFromInfix("= MAX(sheet1!A1:A2)", null);
        Assert.assertEquals(formula.toString(), "MAX(sheet1!A1:A2)");
    }

    @Test
    public void minFormulaSimpleRangeCreation() {
        Formula formula = FormulaCreator.createFormulaFromInfix("= MIN(sheet1!A1:A2)", null);
        Assert.assertEquals(formula.toString(), "MIN(sheet1!A1:A2)");
    }

    @Test
    public void averageFormulaSimpleRangeCreation() {
        Formula formula = FormulaCreator.createFormulaFromInfix("= AVERAGE(sheet1!A1:A2)", null);
        Assert.assertEquals(formula.toString(), "AVERAGE(sheet1!A1:A2)");
    }

}
