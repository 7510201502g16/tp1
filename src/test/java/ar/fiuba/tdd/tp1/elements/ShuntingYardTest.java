package ar.fiuba.tdd.tp1.elements;

import ar.fiuba.tdd.tp1.formulas.notationconverter.NotationConverter;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by matias on 9/25/15.
 */
public class ShuntingYardTest {

    @Test
    public void simpleConversion() {
        String resultado = NotationConverter.convertToPosfix("2 + 2");
        Assert.assertEquals(resultado, "2 2 + ");
    }

    @Test
    public void noParenthesisConversion() {
        String resultado = NotationConverter.convertToPosfix("2 + 2 * 2");
        Assert.assertEquals(resultado, "2 2 2 * + ");
    }

    @Test
    public void parenthesisConversion() {
        String resultado = NotationConverter.convertToPosfix("( 2 + 3 ) * 4");
        Assert.assertEquals(resultado, "2 3 + 4 * ");
    }
}
