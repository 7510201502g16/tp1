package ar.fiuba.tdd.tp1.elements;

import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.cell.CellIdIterator;
import ar.fiuba.tdd.tp1.elements.cell.CellRange;
import org.junit.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.assertEquals;

/**
 * Cell Tests
 * Created by jonathan on 3/10/15.
 */
public class CellIdIteratorTest {

    @Test
    public void iterateSequence() {
        Iterator<String> cellIdIterator = new CellIdIterator(new CellRange("A", 1, "C", 3));
        assertEquals("A1", cellIdIterator.next());
        assertEquals("B1", cellIdIterator.next());
        assertEquals("C1", cellIdIterator.next());
        assertEquals("A2", cellIdIterator.next());
        assertEquals("B2", cellIdIterator.next());
        assertEquals("C2", cellIdIterator.next());
        assertEquals("A3", cellIdIterator.next());
        assertEquals("B3", cellIdIterator.next());
        assertEquals("C3", cellIdIterator.next());
    }

    @Test
    public void hasNextCount() {
        Iterator<String> cellIdIterator = new CellIdIterator(new CellRange("A", 1, "C", 3));
        Integer count = 0;
        while (cellIdIterator.hasNext()) {
            count++;
            cellIdIterator.next();
        }
        assertEquals(Integer.valueOf(9), count);
    }

    @Test(expected = NoSuchElementException.class)
    public void hasNextTooManyTimes() {
        Iterator<String> cellIdIterator = new CellIdIterator(new CellRange("A", 1, "C", 3));
        while (cellIdIterator.hasNext()) {
            cellIdIterator.next();
        }
        cellIdIterator.next();
    }

    @Test
    public void newCellTest() {
        Cell cell = new Cell("", 1);
        assertEquals("0.0", cell.getValue());
        assertEquals("0.0", cell.toString());
    }


    @Test
    public void iterateSequenceColGT26() {
        Iterator<String> cellIdIterator = new CellIdIterator(new CellRange("D", 1, "AA", 1));
        List<String> sequence = Arrays.asList("D1", "E1", "F1", "G1", "H1", "I1", "J1",
                "K1", "L1", "M1", "N1", "O1", "P1", "Q1", "R1", "S1", "T1",
                "U1", "V1", "W1", "X1", "Y1", "Z1", "AA1");

        for (String element : sequence) {
            assertEquals(element, cellIdIterator.next());
        }

    }

    @Test
    public void iterateSequenceColBigger() {
        Iterator<String> cellIdIterator = new CellIdIterator(new CellRange("AAA", 1, "AAAAA", 1));
        Integer finalCount = 26 * 26 * 26 * 26 + 26 * 26 * 26 + 1;
        Integer counter = 0;
        String next = "";
        while (cellIdIterator.hasNext()) {
            counter++;
            next = cellIdIterator.next();
        }

        assertEquals(finalCount, counter);
        assertEquals("AAAAA1", next);
    }


}
