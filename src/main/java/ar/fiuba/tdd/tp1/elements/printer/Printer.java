package ar.fiuba.tdd.tp1.elements.printer;

/**
 * Created by jonathan on 10/10/15.
 */
public interface Printer {

    void print();

}
