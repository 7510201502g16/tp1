package ar.fiuba.tdd.tp1.elements.cell;

import ar.fiuba.tdd.tp1.elements.reference.ReferenceResolver;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;

/**
 * Find the cell that is requestes and if doesn't find it, it does not create it.
 * DO NOT USE WHEN YOU ARE GOING TO WRITE ON THE CELL RETURNED
 * Created by Matias on 08/10/15.
 */
public class ReadOnlyCellReference extends CellReference {

    public ReadOnlyCellReference(ReferenceResolver<Sheet> sheetReference, String row, Integer column) {
        super(sheetReference, row, column);
    }

    @Override
    protected Cell solveCell() {
        final Sheet sheet = sheetReference.solve();
        if (!sheet.cellExists(column, row)) {
            return new Cell(column, row);
        } else {
            return sheet.getCell(column, row);
        }
    }
}
