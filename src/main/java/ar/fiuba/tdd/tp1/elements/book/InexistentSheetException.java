package ar.fiuba.tdd.tp1.elements.book;

/**
 * Exception thrown when there sheet does not exist.
 * Created by jonathan on 23/09/15.
 */
public class InexistentSheetException extends RuntimeException {
}
