package ar.fiuba.tdd.tp1.elements.cell;

import ar.fiuba.tdd.tp1.elements.reference.ReferenceResolver;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;

/**
 * Find the cell that is request
 * Created by jonathan on 26/09/15.
 */
public class ReadWriteCellReference extends CellReference {

    public ReadWriteCellReference(ReferenceResolver<Sheet> sheetReference, String col, Integer row) {
        super(sheetReference, col, row);
    }

    @Override
    protected Cell solveCell() {
        final Sheet sheet = sheetReference.solve();
        return sheet.getCell(column, row);
    }

}
