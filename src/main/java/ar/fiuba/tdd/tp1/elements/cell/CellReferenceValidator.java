package ar.fiuba.tdd.tp1.elements.cell;

import ar.fiuba.tdd.tp1.elements.reference.ReferenceResolver;
import org.jgrapht.experimental.dag.DirectedAcyclicGraph;
import org.jgrapht.graph.DefaultEdge;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by jonathan on 15/10/15.
 */
public class CellReferenceValidator {


    private static CellReferenceValidator instance;
    private Set<ReferenceResolver<Cell>> stackTrace;
    private DirectedAcyclicGraph<String, DefaultEdge> graph;
    private List<String> neighbords;

    public CellReferenceValidator() {
        reset();
    }

    private void reset() {
        stackTrace = new HashSet<>();
        neighbords = new ArrayList<>();
        graph = new DirectedAcyclicGraph<>(DefaultEdge.class);
    }

    public static CellReferenceValidator instance() {
        if (instance == null) {
            instance = new CellReferenceValidator();
        }
        return instance;
    }

    public void startVerification(ReferenceResolver<Cell> ref) {
        if (stackTrace == null || graph == null) {
            throw new IllegalStateException();
        }
        neighbords.add(0, ref.toString());
        graph.addVertex(ref.toString());


        if (neighbords.size() > 1) {
            String actual = neighbords.get(1);
            try {
                graph.addEdge(actual, ref.toString());
            } catch (IllegalArgumentException e) {
                reset();
                throw new CyclicReferenceException();
            }
        }
    }

    public void endVerification(ReferenceResolver<Cell> ref) {
        neighbords.remove(ref.toString());
        //restarts if ends cyclick
        if (neighbords.size() == 0) {
            graph = new DirectedAcyclicGraph<>(DefaultEdge.class);
        }
    }

}
