package ar.fiuba.tdd.tp1.elements.reference;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by jonathan on 12/10/15.
 */
public class ColumnComparator implements Comparator<String>, Serializable {

    @Override
    public int compare(String o1, String o2) {
        Integer lengthCompare = Integer.valueOf(o1.length()).compareTo(Integer.valueOf(o2.length()));
        return colCompare(o1, o2, lengthCompare);

    }

    private Integer colCompare(String col, String other, Integer lengthCompare) {
        if (lengthCompare == 0) {
            return col.compareTo(other);
        }
        return lengthCompare;
    }

}
