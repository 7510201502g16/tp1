package ar.fiuba.tdd.tp1.userinterface.celloptions;

import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.actions.factory.ActionFactory;
import ar.fiuba.tdd.tp1.api.SpreadSheet;

/**
 * Created by maxi on 19/10/15.
 */
public class ChangeValueOption extends CellOption {


    public ChangeValueOption(SpreadSheet spreadSheet) {
        super("Change cell value", "Pleas enter a valid formula", true, spreadSheet);
    }

    @Override
    protected String executeSafeErrors(String input) {
        final Action action = ActionFactory.cellAction(this.col, this.row, spreadSheet.getActiveSheetId()).changeFormula(input);
        spreadSheet.execute(action);
        return "";
    }

}
