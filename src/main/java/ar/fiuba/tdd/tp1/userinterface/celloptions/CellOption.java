package ar.fiuba.tdd.tp1.userinterface.celloptions;

import ar.fiuba.tdd.tp1.api.IllegalActionException;
import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.elements.cell.CellUtilities;
import ar.fiuba.tdd.tp1.userinterface.MenuOption;

/**
 * Created by jonathan on 22/10/15.
 */
public abstract class CellOption extends MenuOption {

    public static final String CELL_INPUT_MESSAGE = "Please enter Cell (EX: A20)";
    public static final String INVALID_CELL_MESSAGE = "Invalid Cell format: EX: SHEET!A20";
    protected Integer row;
    protected String col;
    protected SpreadSheet spreadSheet;
    private String cellInputString;
    private Boolean cellNeedInput;

    public CellOption(String title, String inputMessage, Boolean needInput, SpreadSheet spreadSheet) {
        super(title, CELL_INPUT_MESSAGE, true);
        row = null;
        col = null;
        cellInputString = inputMessage;
        cellNeedInput = needInput;
        this.spreadSheet = spreadSheet;
    }


    @Override
    public MenuOption execute(String input) {
        if (row == null) {
            String result = executeInputCell(input);
            if (result.equals("")) {
                inputMessage = cellInputString;
                if (!cellNeedInput) {
                    this.executeSafeErrors("");
                    return null;
                } else {
                    this.inputMessage = cellInputString;
                }
            }
            return this;
        } else {
            return super.execute(input);
        }
    }

    private String executeInputCell(String input) {
        try {
            row = CellUtilities.getRow(input);
            col = CellUtilities.getCol(input);
        } catch (IllegalActionException e) {
            return INVALID_CELL_MESSAGE;
        }
        return "";
    }

    @Override
    public void reset() {
        super.reset();
        this.inputMessage = CELL_INPUT_MESSAGE;
        this.row = null;
        this.col = null;
    }

    @Override
    public Boolean validateInput(String input) {
        if (col == null) {
            return super.validateInput(input);
        }
        return validateCellInput(input);
    }

    protected Boolean validateCellInput(String input) {
        return true;
    }
}
