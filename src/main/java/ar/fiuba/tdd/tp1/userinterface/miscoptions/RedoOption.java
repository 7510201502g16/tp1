package ar.fiuba.tdd.tp1.userinterface.miscoptions;

import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.userinterface.MenuOption;

/**
 * Created by jonathan on 22/10/15.
 */
public class RedoOption extends MenuOption {

    private SpreadSheet spreadSheet;

    public RedoOption(SpreadSheet spreadSheet) {
        super("Redo", "", false);
        this.spreadSheet = spreadSheet;
    }

    @Override
    protected String executeSafeErrors(String input) {
        spreadSheet.redo();
        return "";
    }
}
