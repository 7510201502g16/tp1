package ar.fiuba.tdd.tp1.userinterface.celloptions;

import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.actions.factory.ActionFactory;
import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.formatter.NumberFormatter;

import java.util.regex.Pattern;

/**
 * Created by maxi on 20/10/15.
 */
public class NumberFormatterOption extends CellOption {


    public NumberFormatterOption(SpreadSheet spreadSheet) {
        super("Number Formatter", "Enter how many decimals do you want", true, spreadSheet);
    }

    @Override
    protected String executeSafeErrors(String input) {
        final NumberFormatter ff = new NumberFormatter(Integer.parseInt(input));
        final Action action = ActionFactory.cellAction(this.col, this.row, this.spreadSheet.getActiveSheetId()).changeFormatter(ff);
        spreadSheet.execute(action);
        return "";
    }

    @Override
    protected Boolean validateCellInput(String input) {
        return Pattern.compile("^[0-9]+$").matcher(input).matches();
    }

}
