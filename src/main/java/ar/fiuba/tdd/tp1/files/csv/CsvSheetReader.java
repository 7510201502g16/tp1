package ar.fiuba.tdd.tp1.files.csv;

import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.actions.factory.ActionFactory;
import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.elements.cell.CellIdIterator;
import ar.fiuba.tdd.tp1.files.ReadException;
import ar.fiuba.tdd.tp1.formulas.formulacreator.FormulaCreator;
import au.com.bytecode.opencsv.CSVReader;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by jonathan on 18/10/15.
 */
public class CsvSheetReader {

    private String book;
    private String fileName;
    private SpreadSheet api;

    public CsvSheetReader(String fileName, String book, SpreadSheet api) {
        this.book = book;
        this.fileName = fileName;
        this.api = api;
    }

    public void importToSheet(String sheetName) {
        api.execute(book, ActionFactory.bookAction().createSheet(sheetName));
        createCells(sheetName);
    }

    private void createCells(String sheet) {
        try (InputStreamReader file = new InputStreamReader(new FileInputStream(fileName), "UTF-8")) {
            CSVReader reader = new CSVReader(file);
            String[] line;
            Integer row = CellIdIterator.firstRow();
            while ((line = reader.readNext()) != null) {
                String col = CellIdIterator.firstCol();
                for (int i = 0; i < line.length; i++) {
                    createCell(sheet, col, row, line[i]);
                    col = CellIdIterator.rollCols(col, 1);
                }
                row = CellIdIterator.rollRow(row, 1);
            }
        } catch (IOException e) {
            throw new ReadException("Cannot open file: " + fileName);
        }

    }

    private void createCell(String sheet, String col, Integer row, String val) {

        if (!FormulaCreator.emptyFormula().evaluate().equals(val)) {
            final Action action = ActionFactory.cellAction(col, row, sheet).changeFormula(val);
            api.execute(book, action);
        }
    }
}