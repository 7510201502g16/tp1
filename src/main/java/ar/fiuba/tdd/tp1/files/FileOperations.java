package ar.fiuba.tdd.tp1.files;

import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.files.csv.CsvSheetReader;
import ar.fiuba.tdd.tp1.files.json.JsonBookReader;

/**
 * Created by jonathan on 18/10/15.
 */
public class FileOperations {

    public static void openBook(String fileName, SpreadSheet api) {
        api.stopActionRecording();
        JsonBookReader.openBook(fileName, api);
        api.startActionRecording();
    }

    public static void importSheet(String fileName, String book, String newSheetName, SpreadSheet api) {
        api.stopActionRecording();
        final CsvSheetReader reader = new CsvSheetReader(fileName, book, api);
        reader.importToSheet(newSheetName);
        api.startActionRecording();
    }
}
