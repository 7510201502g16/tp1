package ar.fiuba.tdd.tp1.printers.csv;

import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.printer.PrintException;
import ar.fiuba.tdd.tp1.elements.printer.Printer;

import java.io.IOException;
import java.io.Writer;

/**
 * Created by maxi on 11/10/15.
 */
public class CSVCellPrinter implements Printer {

    private Writer fileWriter;
    private Cell cell;

    public CSVCellPrinter(Writer fileWriter, Cell cell) {
        this.fileWriter = fileWriter;
        this.cell = cell;
    }

    @Override
    public void print() {
        try {
            this.fileWriter.append(this.cell.getFormattedValue());
        } catch (IOException e) {
            throw new PrintException("Cannot print in file");
        }
    }
}
