package ar.fiuba.tdd.tp1.printers.console;

import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.cell.CellIdIterator;
import ar.fiuba.tdd.tp1.elements.cell.CellRange;
import ar.fiuba.tdd.tp1.elements.printer.Printer;
import ar.fiuba.tdd.tp1.elements.printer.SheetPrinter;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;

import java.util.Iterator;

/**
 * Created by maxi on 12/10/15.
 */
public class ConsoleSheetPrinter extends SheetPrinter {

    public static final Integer COL_COUNT = 10;
    public static final Integer ROW_COUNT = 15;
    private String startingCol;
    private Integer startingRow;


    public ConsoleSheetPrinter(Sheet sheet, String startingCol, Integer startingRow) {
        super(sheet);
        this.startingCol = startingCol;
        this.startingRow = startingRow;
    }

    @Override
    protected void printHeader(Sheet sheet) {
        System.out.println("Sheet: " + sheet.getName());
        CellIdIterator it = new CellIdIterator(startingCol, startingRow, COL_COUNT, 1);
        ConsoleCellPrinter.printAsCell("Row/Col");
        System.out.print("||");
        while (it.hasNext()) {
            it.next();
            ConsoleCellPrinter.printAsCell(it.getCurrentColumn());
            if (it.hasNext()) {
                printCellSeparator();
            }
        }
        System.out.println("");
    }

    @Override
    protected void printRowEnd() {
        System.out.println();
    }

    @Override
    protected void printRowStart(Integer rowNumber) {
        ConsoleCellPrinter.printAsCell(rowNumber.toString());
        System.out.print("||");
    }

    @Override
    protected Printer getCellPrinter(Cell cell) {
        return new ConsoleCellPrinter(cell);
    }

    @Override
    protected Iterator<Iterator<Cell>> cellIterator(Sheet sheet) {
        String endingCol = CellIdIterator.rollCols(startingCol, COL_COUNT - 1);
        Integer endingRow = CellIdIterator.rollRow(startingRow, ROW_COUNT - 1);
        return sheet.rangeIterator(new CellRange(startingCol, startingRow, endingCol, endingRow));
    }

    @Override
    protected void printCellSeparator() {
        System.out.print("|");
    }
}
