package ar.fiuba.tdd.tp1.formulas;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.book.InexistentSheetException;
import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.cell.CellReferenceValidator;
import ar.fiuba.tdd.tp1.elements.cell.Formula;
import ar.fiuba.tdd.tp1.elements.cell.ReadWriteCellReference;
import ar.fiuba.tdd.tp1.elements.reference.ReferenceResolver;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;
import ar.fiuba.tdd.tp1.elements.sheet.SheetReference;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Formula representing a referemce to a cell.
 * Created by matias on 9/24/15.
 */
public class ReferenceFormula implements Formula {

    private static Pattern pattern = Pattern.compile("^([a-zA-Z]+)([\\d]+)$");
    private ReferenceResolver<Cell> cellReference;

    public ReferenceFormula(String reference, Book book) {
        String[] split = reference.split("!");
        String sheetId = split[0];
        String cellId = split[1];
        String row;
        Integer col;
        Matcher matcher = pattern.matcher(cellId);
        if (matcher.find()) {
            row = matcher.group(1);
            col = Integer.parseInt(matcher.group(2));
        } else {
            throw new InvalidFormulaException("Reference not resolved.");
        }
        ReferenceResolver<Sheet> sheetReference = new SheetReference(sheetId, book);
        cellReference = new ReadWriteCellReference(sheetReference, row, col);
    }

    @Override
    public String evaluate() {
        try {
            final CellReferenceValidator validator = CellReferenceValidator.instance();
            validator.startVerification(cellReference);
            Cell cell = cellReference.solve();
            final String value = cell.getValue();
            validator.endVerification(cellReference);
            return value;
        } catch (InexistentSheetException e) {
            throw new InvalidFormulaException("Sheet not found");
        }
    }

    @Override
    public String toString() {
        return this.cellReference.toString();
    }
}
