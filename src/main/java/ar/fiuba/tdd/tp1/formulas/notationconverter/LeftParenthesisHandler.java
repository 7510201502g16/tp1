package ar.fiuba.tdd.tp1.formulas.notationconverter;

import java.util.Deque;

/**
 * Handle the left parenthesis
 * Created by jonathan on 26/09/15.
 */
public class LeftParenthesisHandler extends TokenHandler {

    @Override
    public void handle(String token) {
        if (token.equals("(")) {
            getStack().push(token);
            return;
        }
        next(token);
    }

    public LeftParenthesisHandler(StringBuilder output, Deque<String> stack) {
        super(output, stack);
        setNext(new RightParenthesisHandler(output, stack));
    }

}
