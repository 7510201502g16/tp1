package ar.fiuba.tdd.tp1.formulas.formulacreator;

import ar.fiuba.tdd.tp1.elements.cell.Formula;
import ar.fiuba.tdd.tp1.formulas.BinaryFormula;
import ar.fiuba.tdd.tp1.formulas.BinaryOperation;
import ar.fiuba.tdd.tp1.formulas.InvalidFormulaException;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Link of the chain that handles the operations.
 * Created by matias on 9/26/15.
 */
public class OperationHandler extends CharHandler {

    private Map<String, BinaryOperation> allowedOperations;

    public OperationHandler(Stack<Formula> formulaTree) {
        super(formulaTree);
        this.allowedOperations = new HashMap<>();
        allowedOperations.put("+", BinaryOperation.ADDITION);
        allowedOperations.put("-", BinaryOperation.SUBTRACTION);
    }

    @Override
    public void handle(String string) {
        if (this.allowedOperations.containsKey(string)) {
            if (this.getFormulaTree().size() < 2) {
                throw new InvalidFormulaException("Not enough operands to create BinaryFormula.");
            }
            Formula secondOperand = getFormulaTree().pop();
            BinaryOperation operation = this.allowedOperations.get(string);
            getFormulaTree().push(new BinaryFormula(getFormulaTree().pop(), secondOperand, operation));
            return;
        }
        next(string);
    }
}
