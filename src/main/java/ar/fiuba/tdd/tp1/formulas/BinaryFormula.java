package ar.fiuba.tdd.tp1.formulas;

import ar.fiuba.tdd.tp1.elements.cell.Formula;

/**
 * Generic formula that takes to operands.
 * Created by matias on 9/24/15.
 */
public class BinaryFormula implements Formula {

    protected Formula firstOperand;
    protected Formula secondOperand;
    protected BinaryOperation operator;

    public BinaryFormula(Formula firstOperand, Formula secondOperand, BinaryOperation operation) {
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
        this.operator = operation;
    }

    @Override
    public String toString() {
        return concatWithMiddleSpace(concatWithMiddleSpace(firstOperand.toString(), this.operator.toString()), secondOperand.toString());
    }

    @Override
    public String evaluate() {
        Float firstNumber = Float.parseFloat(this.firstOperand.evaluate());
        Float secondNumber = Float.parseFloat(this.secondOperand.evaluate());
        return String.valueOf(this.operator.apply(firstNumber, secondNumber));
    }

    private String concatWithMiddleSpace(String first, String second) {
        return first.concat(" ").concat(second);
    }
}
