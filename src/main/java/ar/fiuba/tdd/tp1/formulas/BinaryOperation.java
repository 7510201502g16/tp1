package ar.fiuba.tdd.tp1.formulas;

/**
 * Enum that contains de operations allowed.
 * Created by matias on 9/25/15.
 */
public enum BinaryOperation {

    ADDITION("+") {
        @Override
        public float apply(float x1, float x2) {
            return x1 + x2;
        }
    },
    SUBTRACTION("-") {
        @Override
        public float apply(float x1, float x2) {
            return x1 - x2;
        }
    };
    // You'd include other operators too...

    private final String text;

    BinaryOperation(String text) {
        this.text = text;
    }


    public abstract float apply(float x1, float x2);

    @Override
    public String toString() {
        return text;
    }
}
