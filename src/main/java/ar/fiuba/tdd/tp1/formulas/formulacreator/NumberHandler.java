package ar.fiuba.tdd.tp1.formulas.formulacreator;

import ar.fiuba.tdd.tp1.elements.cell.Formula;
import ar.fiuba.tdd.tp1.formulas.NumberFormula;

import java.util.Stack;

/**
 * Link of the Formula creator chain that handles the number.
 * Created by matias on 9/26/15.
 */
public class NumberHandler extends CharHandler {

    public NumberHandler(Stack<Formula> formulaTree) {
        super(formulaTree);
    }

    public static Boolean isNumeric(String stringToCheck) {
        return stringToCheck.matches("[-+]?\\d+(\\.\\d+)?");
    }

    @Override
    public void handle(String string) {
        if (isNumeric(string)) {
            getFormulaTree().push(new NumberFormula(Float.parseFloat(string)));
            return;
        }
        next(string);
    }
}
