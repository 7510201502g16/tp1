package ar.fiuba.tdd.tp1.actions;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.cell.ReadWriteCellReference;
import ar.fiuba.tdd.tp1.elements.reference.ReferenceResolver;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;
import ar.fiuba.tdd.tp1.elements.sheet.SheetReference;

/**
 * Actions to be executed on a cell.
 * Created by jonathan on 23/09/15.
 */
public abstract class CellAction extends Action {

    private String sheetId;
    private String row;
    private Integer column;
    private ReferenceResolver<? extends Cell> reference;

    public CellAction(String sheetId, String row, Integer column) {
        setParams(sheetId, row, column);
    }

    private void setParams(String sheetId, String row, Integer column) {
        this.sheetId = sheetId;
        this.row = row;
        this.column = column;
    }

    @Override
    public void setContext(Book book) {
        super.setContext(book);
        ReferenceResolver<Sheet> sheetResolver = new SheetReference(this.sheetId, this.getContext());
        reference = new ReadWriteCellReference(sheetResolver, this.row, this.column);
    }


    protected Cell getCell() {
        return reference.solve();
    }

    @Override
    public void execute() {
        super.execute();
    }

    @Override
    public void undo() {
        super.undo();
    }
}
