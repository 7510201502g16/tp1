package ar.fiuba.tdd.tp1.actions;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.reference.ReferenceResolver;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;
import ar.fiuba.tdd.tp1.elements.sheet.SheetReference;

/**
 * Actions to be executed on a sheet.
 * Created by jonathan on 23/09/15.
 */
public abstract class SheetAction extends Action {

    private String sheetId;
    private ReferenceResolver<? extends Sheet> reference;

    public SheetAction(String sheetId) {
        this.sheetId = sheetId;
        this.reference = null;
    }

    @Override
    public void setContext(Book book) {
        super.setContext(book);
        ReferenceResolver<Sheet> sheetReference = new SheetReference(this.sheetId, this.getContext());
        this.reference = sheetReference;
    }

    protected Sheet getSheet() {
        return reference.solve();
    }
}
