package ar.fiuba.tdd.tp1.actions.factory;

import ar.fiuba.tdd.tp1.actions.Action;

/**
 * Define the methods to be implemented to create book actions
 * Created by jonathan on 28/09/15.
 */
public interface BookActionFactory {

    Action createSheet(String id);
}
