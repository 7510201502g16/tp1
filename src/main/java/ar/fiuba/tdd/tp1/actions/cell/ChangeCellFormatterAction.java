package ar.fiuba.tdd.tp1.actions.cell;

import ar.fiuba.tdd.tp1.actions.CellAction;
import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.formatter.Formatter;

/**
 * Created by jonathan on 15/10/15.
 */
public class ChangeCellFormatterAction extends CellAction {

    private Formatter formatter;
    private Formatter previous;

    public ChangeCellFormatterAction(String sheetId, String row, Integer column, Formatter formatter) {
        super(sheetId, row, column);
        this.formatter = formatter;
    }

    @Override
    public void execute() {
        super.execute();
        final Cell cell = this.getCell();
        previous = cell.getFormatter();
        cell.setFormatter(formatter);
    }

    @Override
    public void undo() {
        super.undo();
        this.getCell().setFormatter(previous);
    }
}
